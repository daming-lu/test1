#!/usr/bin/python
# coding-utf8

'''
Developer: Kaibo(lrushx)
Email: liukaib@oregonstate.edu
Created Date: Aug 15, 2018
'''

from __future__ import division, unicode_literals
from flask import Flask, jsonify, render_template, request
#import flask_cors
from werkzeug.utils import secure_filename
import os
import time
import re
import json
# import psutil



import argparse

from onmt.utils.logging import init_logger
from onmt.translate.translator import build_translator

import onmt.inputters
import onmt.translate
import onmt
import onmt.model_builder
import onmt.modules
import onmt.opts
import tools.apply_bpe as applybpe
import jieba



# last version, make_translator2 is for baseline
#from onmt.translate.Translator import make_translator,make_translator2
#import onmt.io
#import onmt.ModelConstructor
#from tkinter import *
import pinyin
#import torch
#from torch.autograd import Variable
import math
#import speech_recognition as sr
import operator

# from micro_stream import MicrophoneStream
# from google.cloud import speech
# from google.cloud.speech import enums
# from google.cloud.speech import types
from itertools import chain

# from googletrans import Translator as word_trans

import time

app = Flask(__name__)
#flask_cors.CORS(app)
optL = [True]
Dict = {'chnBase':"",'engBase':"",'bslBase':"",
        'chnLine':"",'engLine':"",'bslLine':"",'pinyin':"",'word':"",
        'chnLines':0,'engLines':0,'bslLines':0,
        'biling':""}
        #'chnWid':0,  'engWid':0,  'bslWid':0}

bpe = applybpe.initializebpe("tools/2M.bpedict.zh")
modelStatus = ["model loading..."]
MaxLine = 11
MaxWid  = 28
stream_end = [False]
duration = [0,0]

def main(opt, src_bulk=None):
    freedecoding = [True]
    pre_tgt      = [""]


    #font_align = ('Courier', 20)# 'Roboto Mono''Letter Gothic'#font_align
    #font_label = ('Times',14)

    translator = build_translator(opt, report_score=True)
    word_translator = word_trans()



    def alignment(zh_list,pinyin_list):
        zh = []
        py = []
        for i,j in zip(zh_list,pinyin_list):
            zh_len, pin_len = len(i)*2, len(j)
            len_diff = math.ceil((pin_len - zh_len)*2)
            i = i+" "*len_diff
            zh.append(i)
            py.append(j)

        return " ".join(zh)+"\n"+" ".join(py)
    def count_space(l1,l2,l3):
        ratio = 2
        l1 = int(l1 * ratio)
        w = max(l1,l2,l3) + 1
        if w & 1: w += 1
        return w-l1, w-l2, w-l3


    def trans_zh_en_only(src_txt_list, finishall=False):
        chn_str = ''.join(src_txt_list)

        src_txtidx_list = []
        res_baseline = ""
        res = ""
        # last version
        #src_txtidx_list = [translator.fields["src"].vocab.stoi[x] for x in src_txt_list]
        #translated_sent = translator.translate_batch(src_txtidx_list,translator.model_opt.waitk) # input is word id list

        # 9/26/2018 version
        if len(src_txt_list) >= translator.waitk:
            current_input_bpe_lst = bpe.segment(" ".join(src_txt_list)).split()
            src_txtidx_list = []
            src_txtidx_list = [translator.fields["src"].vocab.stoi[wordi] for wordi in current_input_bpe_lst]

            ### detect the end of sentence
            #if sentwidx == len(sampleWholeInput)-1:
            catchflag = False   # waitk, no catchup
            #catchflag = ((len(pre_tgt[0])+2)%5==0)   # catchup, one more decoding if previous len(tgt) is 3, 8,
            translated_sent, pre_tgt[0], src_x = translator._translate_batch(src_txtidx_list,pre_tgt[0],freedecoding[0],finishall=finishall,catchflag=catchflag)
            print(src_x, '-'*20)
            print(' & '.join(current_input_bpe_lst))#, catchflag, src_x)
            print(' & '.join(translated_sent))

            res = clean_eos(translated_sent)

            if finishall:
                res_baseline_list, pre_tgt[0], _ = translator._translate_batch(src_txtidx_list,pre_tgt[0],freedecoding=True,finishall=True,catchflag=catchflag)
                #res_baseline = " ".join(clean_eos(res_baseline_list)).replace("@@ ","") + '.'
                #pinyin_str += '。'
                #word_str += '.'
                print('\n\nChinese: {}'.format(chn_str))
                print('k-waits:{}\n{}'.format(res, "".join(src_txt_list)))
            freedecoding[0] = False
        else: print('x', '-'*20)
        #print('temp: {} {} {} [{} {}] {}'.format(src_txt_list, res, src_txtidx_list, freedecoding[0], finishall, pre_tgt[0]))
        #print('\rChinese: {}'.format(chn_str), end = '')


        return chn_str, res


    def clean_eos(s):
        if len(s)>=2 and s[-1] == "</s>":       # replace tail ".</s>" or "</s>" to "."
            if s[-2] == ".":
                s = s[:-1]
            else:
                s[-1] = "."
        #res = " ".join(translated_sent)
        return " ".join(s).replace("@@ ","")



    # added 10/15/2018
    def input_print_loop(responses):
        """Iterates through server responses and prints them.

        The responses passed is a generator that will block until a response
        is provided by the server.

        Each response may contain multiple results, and each result may contain
        multiple alternatives; for details, see https://goo.gl/tjCPAU.  Here we
        print only the transcription for the top alternative of the top result.

        In this case, responses are provided for interim results as well. If the
        response is an interim one, print a line feed at the end of it, to allow
        the next result to overwrite it, until the response is a final one. For the
        final one, print a newline to preserve the finalized transcription.
        """


        num_words = 0   # number of words in the whole sentence (maybe multi lines)
        num_chars = 0   # number of char in current line
        #num_chars_printed = 0
        base_txt_list, txt_list = [], None
        #chn_str, pinyin_str, word_str = '','',''
        stream_end[0] = False
        Dict['chnLine'] = ""
        Dict['engLine'] = ""

        for j in range(len(responses)):
            print('-'*2,j+1,end=',')
            txt_list = responses[0:j+1]
            Dict['chnLine'], Dict['engLine'] = trans_zh_en_only(txt_list, finishall=False)



            #print('-'*20, num_chars, num_words, Dict['chnLine'])


            #print('+'*20,transcript, stream_end[0])
            '''
            if len(transcript) > num_chars and ord(transcript[-1]) > 128:   # kaibo: cut only if characters increases & not eng
                num_chars = len(transcript)
                transcript_cut = jieba.cut(transcript, cut_all=False)
                #txt_list = list(w.lower() if ord(w[0])<128 else w for w in chain(base_txt_list, transcript_cut))
                txt_list = list(map(lambda x: x.lower(), chain(base_txt_list, transcript_cut)))
                if len(txt_list) > num_words: # kaibo: update GUI only if words increases
                    num_words = len(txt_list)
                    if num_words > 1:
                        #chn_str, pinyin_str, word_str = trans(txt_list[:-1], chn_str, pinyin_str, word_str)
                        Dict['chnLine'], Dict['engLine'] = trans_zh_en_only(txt_list[:-1])
            #print('-'*20, num_chars, num_words, Dict['chnLine'])
            '''

            #########
            #print('chn:{}{}\neng:{}{}'.format(' '*10,Dict['chnLine'],' '*10,Dict['engLine']))
            if j == len(responses)-1:
            #if asr_result.is_final:
                #trans(txt_list + ['。'], chn_str, pinyin_str, word_str)
                #trans_whole(txt_list + ['。'])
                print('-'*2,'finish',end=',')
                Dict['chnLine'], eng = trans_zh_en_only(txt_list, finishall=True)
                print('finished\n')
                Dict['chnLine'] += '。'
                Dict['engLine'] = eng

                #update_base('chn', Dict['chnLine'].replace('\t','').replace(chr(12288),''), True)
                stream_end[0] = True

                #update_base('bsl', bsl)

                #stream_end[0] = False
                freedecoding[0] = True   # kaibo: added for 8/8/2018 version
                pre_tgt[0] = []         # kaibo: added for 8/8/2018 version
                src_txt_list = []
                num_words = 0   # number of words in the whole sentence (maybe multi lines)
                num_chars = 0   # number of char in current line
                base_txt_list, txt_list = [], None
                #print(". [end]")


    def update_base(key, ischn=False):
        #s1, n = lineBreak(s, ischn=ischn)
        #Dict['biling'] += "<p>{}<br><span{}>{}</span></p>".format(Dict[key1+'Line'],' style="color:#CCFF99;"', Dict[key2+'Line'])
        Dict[key+'Base'] += Dict[key+'Line']
        Dict[key+'Line'] = ""
        #Dict[key+'Lines'] += n
        #Dict[key+'Line'] = ""

        #print('{}:{}'.format(key+'Base', Dict[key+'Base']))


    '''
    # Audio recording parameters
    RATE = 16000
    #RATE = 10000
    CHUNK = int(RATE / 10)  # 100ms
    language_code = "cmn-Hans-CN"
    client = speech.SpeechClient()
    config = types.RecognitionConfig(
        encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
        sample_rate_hertz=RATE,
        enable_automatic_punctuation=True,
        language_code=language_code)
    streaming_config = types.StreamingRecognitionConfig(
        config=config,
        interim_results=True)

    #stream_end[0] = False
    print('say something until a pause as ending in 65 seconds')
    modelStatus[0] = "model loaded sucessfully, say something within"
    duration[0] = time.time()
    t0 = time.time()
    with MicrophoneStream(RATE, CHUNK) as stream:
        audio_generator = stream.generator()
        requests = (types.StreamingRecognizeRequest(audio_content=content)
                    for content in audio_generator)
        responses = client.streaming_recognize(streaming_config, requests)
        # Now, put the transcription responses to use.
        listen_print_loop(responses)
    '''
    stream_end[0] = False
    for chinese in src_bulk:
        input_print_loop(chinese.split())





if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='translate.py',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    onmt.opts.add_md_help_argument(parser)
    onmt.opts.translate_opts(parser)

    opt = parser.parse_args()
    print(opt.model)
    logger = init_logger(opt.log_file)
    optL[0] = opt

    src_bulk = [#'奥运会 将 于 2008 年 在 北京 举行'
                #,'近年来 中国 政府 一直 致力 于 消除 贫困'
                #,'中东 地区 因为 局势 动荡 所以 爆发 了 一 系列 冲突'
                #,'江 泽民 对 布什 的 发言 表示 遗憾'
                '布什 总统 在 莫斯科 与 普京 总统 会晤'
                ,'布什 总统 在 莫斯科 与 普京 会晤'
                ,'布什 总统 在 莫斯科 与 俄罗斯 总统 普京 会晤'
                ,'美国 总统 布什 在 莫斯科 与 俄罗斯 总统 普京 会晤'
                #,'江 泽民 对 美国 总统 的 发言 表示 遗憾'
                #,'戴安娜 王妃 在 1997 年 死 于 一场 巴黎 车祸'
                #,'法国 警方 经过 两年 的 调查 认为 汽车 失控 的 原因 是 司机 喝酒 并且 服用 药物'
                ]
    main(opt, src_bulk)

    '''
    app.logger.info('message processed')
    #app.run(host='0.0.0.0') #, debug=True)
    app.run(debug=True)
    '''
