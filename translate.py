#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, unicode_literals
import argparse

from onmt.utils.logging import init_logger
from onmt.translate.translator import build_translator

import onmt.inputters
import onmt.translate
import onmt
import onmt.model_builder
import onmt.modules
import onmt.opts
import tools.apply_bpe as applybpe
import jieba

bpe = applybpe.initializebpe("tools/2M.bpedict.zh")
def main(opt):

    def clean_eos(s):
        if len(s)>=2 and s[-1] == "</s>":       # replace tail ".</s>" or "</s>" to "."
            if s[-2] == ".":
                s = s[:-1]
            else:
                s[-1] = "."
        #res = " ".join(translated_sent)
        return s



    translator = build_translator(opt, report_score=True)
    
    #sampleinput = ['载@@', '入', '黛', '妃', '死@@', '因', '调查', '资料', '的', '两', '台', '手提', '电脑', '遭', '窃',"。"]
    ##sampleinput = ["斯里兰卡", "交@@", "战", "双方", "同意", "本月", "下旬", "在", "日内瓦", "谈判",]
    
    sampleinput = "载入黛妃死因调查资料的两台手提电脑遭窃"
    sampleinput = "江泽民对美国总统的发言表示遗憾。"
    
    #sampleinput = "中国政府一直致力于消除贫困。"
    #sampleinput = "中国自古以来从未侵犯国任何国家。"
    sampleinput = jieba.lcut(sampleinput, cut_all=False)
    print(sampleinput)
    
    
    
    pre_tgt = []
    freedecoding=True
    finishall = False
    translated_sent = []
    src_txtidx_list = []

    for sentwidx in range(len(sampleinput)):
        
        current_input = sampleinput[:sentwidx+1]
          
        
        if len(current_input) >= translator.waitk:
            
            current_input_bpe_lst = bpe.segment(" ".join(current_input)).split()
            
            src_txtidx_list = []
            for wordi in current_input_bpe_lst:
                src_txtidx_list.append(translator.fields["src"].vocab.stoi[wordi])
            
            if sentwidx == len(sampleinput)-2: continue
            if sentwidx == len(sampleinput)-1:
                finishall=True
            else:
                finishall=False

            translated_sent, pre_tgt = translator._translate_batch(src_txtidx_list,pre_tgt,freedecoding,finishall)
            
            freedecoding = False
            

        print (sentwidx+1, current_input, " ".join(clean_eos(translated_sent)).replace("@@ ",""), '{} [{} {}] {}'.format(src_txtidx_list, freedecoding, finishall, pre_tgt))
    print ("\n"*2, " ".join(clean_eos(translated_sent)).replace("@@ ",""))
    




if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='translate.py',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    onmt.opts.add_md_help_argument(parser)
    onmt.opts.translate_opts(parser)

    opt = parser.parse_args()
    logger = init_logger(opt.log_file)
    main(opt)
