w4_step_80000.pt
-- 1,x --------------------
-- 2,x --------------------
-- 3,x --------------------
-- 4,1 --------------------
奥运会 & 将 & 于 & 2008
the
-- 5,2 --------------------
奥运会 & 将 & 于 & 2008 & 年
the & olympic
-- 6,3 --------------------
奥运会 & 将 & 于 & 2008 & 年 & 在
the & olympic & games
-- 7,5 --------------------
奥运会 & 将 & 于 & 2008 & 年 & 在 & 北京
the & olympic & games & will & be
-- 8,6 --------------------
奥运会 & 将 & 于 & 2008 & 年 & 在 & 北京 & 举行
the & olympic & games & will & be & held
-- finish,12 --------------------
奥运会 & 将 & 于 & 2008 & 年 & 在 & 北京 & 举行
the & olympic & games & will & be & held & in & beijing & in & 2008 & . & </s>


Chinese: 奥运会将于2008年在北京举行
k-waits:the olympic games will be held in beijing in 2008 .
奥运会将于2008年在北京举行
finished

-- 1,x --------------------
-- 2,x --------------------
-- 3,x --------------------
-- 4,1 --------------------
近年来 & 中国 & 政府 & 一直
in
-- 5,2 --------------------
近年来 & 中国 & 政府 & 一直 & 致力
in & recent
-- 6,3 --------------------
近年来 & 中国 & 政府 & 一直 & 致力 & 于
in & recent & years
-- 7,5 --------------------
近年来 & 中国 & 政府 & 一直 & 致力 & 于 & 消除
in & recent & years & , & the
-- 8,6 --------------------
近年来 & 中国 & 政府 & 一直 & 致力 & 于 & 消除 & 贫困
in & recent & years & , & the & chinese
-- finish,15 --------------------
近年来 & 中国 & 政府 & 一直 & 致力 & 于 & 消除 & 贫困
in & recent & years & , & the & chinese & government & has & been & working & to & eliminate & poverty & . & </s>


Chinese: 近年来中国政府一直致力于消除贫困
k-waits:in recent years , the chinese government has been working to eliminate poverty .
近年来中国政府一直致力于消除贫困
finished

-- 1,x --------------------
-- 2,x --------------------
-- 3,x --------------------
-- 4,1 --------------------
中东 & 地区 & 因为 & 局势
the
-- 5,2 --------------------
中东 & 地区 & 因为 & 局势 & 动荡
the & middle
-- 6,3 --------------------
中东 & 地区 & 因为 & 局势 & 动荡 & 所以
the & middle & east
-- 7,5 --------------------
中东 & 地区 & 因为 & 局势 & 动荡 & 所以 & 爆发
the & middle & east & has & suffered
-- 8,6 --------------------
中东 & 地区 & 因为 & 局势 & 动荡 & 所以 & 爆发 & 了
the & middle & east & has & suffered & a
-- 9,7 --------------------
中东 & 地区 & 因为 & 局势 & 动荡 & 所以 & 爆发 & 了 & 一@@ & 系列
the & middle & east & has & suffered & a & series
-- 10,9 --------------------
中东 & 地区 & 因为 & 局势 & 动荡 & 所以 & 爆发 & 了 & 一@@ & 系列 & 冲突
the & middle & east & has & suffered & a & series & of & conflicts
-- finish,21 --------------------
中东 & 地区 & 因为 & 局势 & 动荡 & 所以 & 爆发 & 了 & 一@@ & 系列 & 冲突
the & middle & east & has & suffered & a & series & of & conflicts & due & to & the & tur@@ & bul@@ & ence & in & the & middle & east & . & </s>


Chinese: 中东地区因为局势动荡所以爆发了一系列冲突
k-waits:the middle east has suffered a series of conflicts due to the turbulence in the middle east .
中东地区因为局势动荡所以爆发了一系列冲突
finished

-- 1,x --------------------
-- 2,x --------------------
-- 3,x --------------------
-- 4,1 --------------------
江 & 泽民 & 对 & 美国
jiang
-- 5,2 --------------------
江 & 泽民 & 对 & 美国 & 总统
jiang & zemin
-- 6,3 --------------------
江 & 泽民 & 对 & 美国 & 总统 & 的
jiang & zemin & expressed
-- 7,5 --------------------
江 & 泽民 & 对 & 美国 & 总统 & 的 & 发言
jiang & zemin & expressed & his & appreciation
-- 8,6 --------------------
江 & 泽民 & 对 & 美国 & 总统 & 的 & 发言 & 表示
jiang & zemin & expressed & his & appreciation & for
-- 9,7 --------------------
江 & 泽民 & 对 & 美国 & 总统 & 的 & 发言 & 表示 & 遗憾
jiang & zemin & expressed & his & appreciation & for & the
-- finish,13 --------------------
江 & 泽民 & 对 & 美国 & 总统 & 的 & 发言 & 表示 & 遗憾
jiang & zemin & expressed & his & appreciation & for & the & us & president & 's & remarks & . & </s>


Chinese: 江泽民对美国总统的发言表示遗憾
k-waits:jiang zemin expressed his appreciation for the us president 's remarks .
江泽民对美国总统的发言表示遗憾
finished

