w5_step_150000.pt
-- 1,x --------------------
-- 2,x --------------------
-- 3,x --------------------
-- 4,x --------------------
-- 5,1 --------------------
奥运会 & 将 & 于 & 2008 & 年
the
-- 6,2 --------------------
奥运会 & 将 & 于 & 2008 & 年 & 在
the & olympic
-- 7,3 --------------------
奥运会 & 将 & 于 & 2008 & 年 & 在 & 北京
the & olympic & games
-- 8,4 --------------------
奥运会 & 将 & 于 & 2008 & 年 & 在 & 北京 & 举行
the & olympic & games & will
-- finish,12 --------------------
奥运会 & 将 & 于 & 2008 & 年 & 在 & 北京 & 举行
the & olympic & games & will & be & held & in & beijing & in & 2008 & . & </s>


Chinese: 奥运会将于2008年在北京举行
k-waits:the olympic games will be held in beijing in 2008 .
奥运会将于2008年在北京举行
finished

-- 1,x --------------------
-- 2,x --------------------
-- 3,x --------------------
-- 4,x --------------------
-- 5,1 --------------------
近年来 & 中国 & 政府 & 一直 & 致力
in
-- 6,2 --------------------
近年来 & 中国 & 政府 & 一直 & 致力 & 于
in & recent
-- 7,3 --------------------
近年来 & 中国 & 政府 & 一直 & 致力 & 于 & 消除
in & recent & years
-- 8,4 --------------------
近年来 & 中国 & 政府 & 一直 & 致力 & 于 & 消除 & 贫困
in & recent & years & ,
-- finish,16 --------------------
近年来 & 中国 & 政府 & 一直 & 致力 & 于 & 消除 & 贫困
in & recent & years & , & the & chinese & government & has & been & working & hard & to & eliminate & poverty & . & </s>


Chinese: 近年来中国政府一直致力于消除贫困
k-waits:in recent years , the chinese government has been working hard to eliminate poverty .
近年来中国政府一直致力于消除贫困
finished

-- 1,x --------------------
-- 2,x --------------------
-- 3,x --------------------
-- 4,x --------------------
-- 5,1 --------------------
中东 & 地区 & 因为 & 局势 & 动荡
the
-- 6,2 --------------------
中东 & 地区 & 因为 & 局势 & 动荡 & 所以
the & middle
-- 7,3 --------------------
中东 & 地区 & 因为 & 局势 & 动荡 & 所以 & 爆发
the & middle & east
-- 8,4 --------------------
中东 & 地区 & 因为 & 局势 & 动荡 & 所以 & 爆发 & 了
the & middle & east & region
-- 9,5 --------------------
中东 & 地区 & 因为 & 局势 & 动荡 & 所以 & 爆发 & 了 & 一@@ & 系列
the & middle & east & region & has
-- 10,6 --------------------
中东 & 地区 & 因为 & 局势 & 动荡 & 所以 & 爆发 & 了 & 一@@ & 系列 & 冲突
the & middle & east & region & has & experienced
-- finish,21 --------------------
中东 & 地区 & 因为 & 局势 & 动荡 & 所以 & 爆发 & 了 & 一@@ & 系列 & 冲突
the & middle & east & region & has & experienced & a & series & of & conflicts & due & to & the & tur@@ & bul@@ & ence & of & the & situation & . & </s>


Chinese: 中东地区因为局势动荡所以爆发了一系列冲突
k-waits:the middle east region has experienced a series of conflicts due to the turbulence of the situation .
中东地区因为局势动荡所以爆发了一系列冲突
finished

-- 1,x --------------------
-- 2,x --------------------
-- 3,x --------------------
-- 4,x --------------------
-- 5,1 --------------------
江 & 泽民 & 对 & 布什 & 的
jiang
-- 6,2 --------------------
江 & 泽民 & 对 & 布什 & 的 & 发言
jiang & zemin
-- 7,3 --------------------
江 & 泽民 & 对 & 布什 & 的 & 发言 & 表示
jiang & zemin & expressed
-- 8,4 --------------------
江 & 泽民 & 对 & 布什 & 的 & 发言 & 表示 & 遗憾
jiang & zemin & expressed & regret
-- finish,9 --------------------
江 & 泽民 & 对 & 布什 & 的 & 发言 & 表示 & 遗憾
jiang & zemin & expressed & regret & over & bush & 's & speech & </s>


Chinese: 江泽民对布什的发言表示遗憾
k-waits:jiang zemin expressed regret over bush 's speech .
江泽民对布什的发言表示遗憾
finished

