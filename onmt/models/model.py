""" Onmt NMT Model base class definition """
import torch.nn as nn
import torch

class NMTModel(nn.Module):
    """
    Core trainable object in OpenNMT. Implements a trainable interface
    for a simple, generic encoder + decoder model.

    Args:
      encoder (:obj:`EncoderBase`): an encoder object
      decoder (:obj:`RNNDecoderBase`): a decoder object
      multi<gpu (bool): setup for multigpu support
    """

    def __init__(self, encoder, decoder, multigpu=False):
        self.multigpu = multigpu
        super(NMTModel, self).__init__()
        self.encoder = encoder
        self.decoder = decoder

    def forward(self, src, tgt, lengths, waitk, ngpu, dec_state=None):
        """Forward propagate a `src` and `tgt` pair for training.
        Possible initialized with a beginning decoder state.

        Args:
            src (:obj:`Tensor`):
                a source sequence passed to encoder.
                typically for inputs this will be a padded :obj:`LongTensor`
                of size `[len x batch x features]`. however, may be an
                image or other generic input depending on encoder.
            tgt (:obj:`LongTensor`):
                 a target sequence of size `[tgt_len x batch]`.
            lengths(:obj:`LongTensor`): the src lengths, pre-padding `[batch]`.
            dec_state (:obj:`DecoderState`, optional): initial decoder state
        Returns:
            (:obj:`FloatTensor`, `dict`, :obj:`onmt.Models.DecoderState`):

                 * decoder output `[tgt_len x batch x hidden]`
                 * dictionary attention dists of `[tgt_len x batch x src_len]`
                 * final decoder state
        """
        tgt = tgt[:-1]  # exclude last target from inputs
        src_len, batchsize, _ = src.shape
        
        if waitk >= src_len:
            enc_final, memory_bank = self.encoder(src, lengths)
            enc_state = self.decoder.init_decoder_state(src, memory_bank, enc_final)
            decoder_outputs, dec_state, attns = \
                        self.decoder(tgt, memory_bank,[],
                                     enc_state if dec_state is None
                                     else dec_state,ngpu,
                                     memory_lengths=lengths)
            
        else:
            context_list = []
            hidden_list = []        
            for i in range(waitk,src_len+1):
                current_length=[]
                for len_idx in lengths:
                    if i < len_idx:
                        current_length.append(i)
                    else:
                        current_length.append(len_idx)
        
                if ngpu>0:
                    temp_lengths = torch.LongTensor(current_length).cuda()
                else:
                    temp_lengths = torch.LongTensor(current_length)
        
                enc_final, memory_bank = self.encoder(src[:i,:,:], temp_lengths)
                context_list.append(memory_bank)
                hidden_list.append(enc_final)      
                
            #enc_final = hidden_list[0]
            enc_state = self.decoder.init_decoder_state(src, context_list[0], hidden_list[0])    
            
        
        
            decoder_outputs, dec_state, attns = \
                self.decoder(tgt, memory_bank,context_list,
                             enc_state if dec_state is None
                             else dec_state,ngpu,
                             memory_lengths=lengths)
        if self.multigpu:
            # Not yet supported on multi-gpu
            dec_state = None
            attns = None
        return decoder_outputs, attns, dec_state
