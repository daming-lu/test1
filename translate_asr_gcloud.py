#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, unicode_literals
import argparse

from onmt.utils.logging import init_logger
from onmt.translate.translator import build_translator

import onmt.inputters
import onmt.translate
import onmt
import onmt.model_builder
import onmt.modules
import onmt.opts
import tools.apply_bpe as applybpe
import jieba


# last version, make_translator2 is for baseline
#from onmt.translate.Translator import make_translator,make_translator2
#import onmt.io
#import onmt.ModelConstructor
from tkinter import *
import pinyin
#import torch
#from torch.autograd import Variable
import math
#import speech_recognition as sr
import operator

from micro_stream import MicrophoneStream
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types
from itertools import chain

from googletrans import Translator as word_trans


bpe = applybpe.initializebpe("tools/2M.bpedict.zh")

def main(opt):
    # following 2 lines for gloss dict
    #dicte2f = pickle.load(open("gloss/gloss_dicte2f.pkl","rb"))
    #dictf2e = pickle.load(open("gloss/gloss_dictf2e.pkl","rb"))

    #translator = make_translator(opt, report_score=True)
    #opt.model = opt.basemodel
    #translator2 = make_translator2(opt, report_score=True)
    
    #translator2.translate(opt.src_dir, opt.src, opt.tgt,
                             #opt.batch_size, opt.attn_debug)    
    
    translator = build_translator(opt, report_score=True)

    freedecoding = [True]
    pre_tgt      = [True]


    font_align = ('Courier', 20)# 'Roboto Mono''Letter Gothic'#font_align
    font_label = ('Times',14)
    
    window = Tk()
    
    #modelid = " ("+opt.model[opt.model.index("_acc")-2:]+")"
    waitnum = str(translator.waitk)
    modelid = ' ('+opt.model+')'
    
    window.title("Welcome to NEW Simultaneous Translation" + modelid)
    window.geometry('1500x350')
    image = PhotoImage(file='download.png')
    
    button0 = Label(window,height=5)
    button0.grid(column=1, row=0)
    
    button = Label(window, image=image)
    #button.grid(column=1, row=0)
    button.place(x=550, y=10) 
    
    # for chinese input
    lbl2 = Label(window, text="Chinese input:",fg="green",font = font_label,anchor=E, width=28)
    lbl2.grid(column=0, row=1)    
    txt = Entry(window,width=130,font = ('Courier', 24))
    txt.grid(column=1, row=1)
    txt.focus()
    
    # for pinyin outputs
    lbl_s = Label(window, text="", width=150,font = font_align, background="pale green", anchor=W, justify=LEFT)
    lbl_s.grid(column=1, row=2)   
    lbl1_s = Label(window, text="Pinyin:",fg="black",font = font_label,anchor=E, width=28)
    lbl1_s.grid(column=0, row=2)
    
    # for gloss/word-by-word outputs
    lbl_gs = Label(window, text="", width=150,font = font_align, background="pale green", anchor=W, justify=LEFT)
    lbl_gs.grid(column=1, row=3)   
    lbl1_gs = Label(window, text="Word-by-Word\nTranslation:",fg="black",font = font_label,anchor=E, width=28)
    lbl1_gs.grid(column=0, row=3)   


    # for Simultaneous MT outputs
    lbl = Label(window, text="", width=150,font = font_align, background="linen")
    lbl.grid(column=1, row=4)   
    lbl1 = Label(window, text="Simultaneous\nTranslation (wait "+waitnum+"):",fg="red",font = font_label,anchor=E, width=28)
    lbl1.grid(column=0, row=4)    
    
    
    # for baseline outputs
    lbl_baseline = Label(window, text="", width=150,font = font_align, background="sky blue")
    lbl_baseline.grid(column=1, row=5) 
    #lbl1_baseline = Label(window, text="Baseline\nTranlation (gready):",fg="blue",font = font_label,anchor=E, width=28)
    lbl1_baseline = Label(window, text="Baseline:",fg="blue",font = font_label,anchor=E, width=28)
    lbl1_baseline.grid(column=0, row=5)    
    
#    # for baseline outputs with beam
#    lbl_baseline_beam = Label(window, text="", width=150,font = font_align, background="sky blue")
#    lbl_baseline_beam.grid(column=1, row=6) 
#    lbl1_baseline_beam = Label(window, text="Baseline\nTranlation (beam 5):",fg="blue",font = font_label,anchor=E, width=28)
#    lbl1_baseline_beam.grid(column=0, row=6)     
    
    
    act_click = '<space>'
    word_translator = word_trans()

    def clearUI():
        txt.delete(0,END)
        lbl.configure(text= "", fg="red", anchor=W, justify=LEFT)
        lbl_baseline.configure(text= "", fg="blue", anchor=W, justify=LEFT)
        #lbl_baseline_beam.configure(text= "", fg="blue", anchor=W, justify=LEFT)
        lbl_s.configure(text= "", fg="black", anchor=W, justify=LEFT)
        lbl_gs.configure(text= "", fg="black", anchor=W, justify=LEFT)
        window.update()

    def alignment(zh_list,pinyin_list):
        zh = []
        py = []
        for i,j in zip(zh_list,pinyin_list):
            zh_len, pin_len = len(i)*2, len(j)
            len_diff = math.ceil((pin_len - zh_len)*2)
            i = i+" "*len_diff
            zh.append(i)
            py.append(j)
            
        return " ".join(zh)+"\n"+" ".join(py)
    def count_space(l1,l2,l3):
        ratio = 2
        l1 = int(l1 * ratio)
        w = max(l1,l2,l3) + 1
        if w & 1: w += 1
        return w-l1, w-l2, w-l3

    """
    # trans is incremental append
    # trans_whole is start over with whole new sentence
    def trans(src_txt_list, chn_str='', pinyin_str='', word_str=''):
        ''' 
        inputtext = txt.get()          # kaibo: keyboard mode, inputtext is a text increased word by word against time

        
        #segments = ""
        
        #if opt.usesegment:
            #seg_list = jieba.cut(inputtext, cut_all=False)
            #segments1 = [i for i in seg_list]
            #newinputtext = " ".join(segments1).strip()
            #inputtext = newinputtext
            #segments = newinputtext
            
        #pinyin_list = []
            
        src_txt_list = inputtext.strip().split()
        '''

        #src_txt_list.append(word)       # kaibo: speech mode, listen to a word until '句号'

        
        new_word = src_txt_list[-2] if src_txt_list[-1] == '。' else src_txt_list[-1]
        new_pinyin = pinyin.get(new_word)
        new_trans_word = word_translator.translate(new_word,dest='en').text.lower()

        #while len(out_gloss_list) < len(src_txt_list): out_gloss_list.append('<unk>')
        #while len(pinyin_list) < len(src_txt_list): pinyin_list.append('<unk>')
        #hile len(out_word_list) < len(src_txt_list): out_word_list.append('<unk>')
        
        BLANK = ' '
        if ord(new_word[0]) > 122:
            x,y,z = count_space(len(new_word), len(new_pinyin), len(new_trans_word))
            new_chn_str  = new_word + chr(12288)*(x>>1) #chr(12288)
            pinyin_str  += new_pinyin + BLANK*y
            word_str    += new_trans_word + BLANK*z
        else:
            new_chn_str  = new_word + '\t'
            pinyin_str  += new_pinyin + '\t'
            word_str    += new_trans_word + '\t'
        txt.insert(END, new_chn_str)
        chn_str += new_chn_str
        
        window.update()

        print('\rChinese: {}'.format(chn_str), end = '')

        src_txtidx_list = [translator.fields["src"].vocab.stoi[x] for x in src_txt_list]
        #for i in src_txt_list:
        #    src_txtidx_list.append(translator.fields["src"].vocab.stoi[i])            

        translated_sent = translator.translate_batch(src_txtidx_list,translator.model_opt.waitk) # input is word id list
        
        if src_txtidx_list[-1] == 4:
            translated_sent_baseline_beam = translator2.translate_beam(src_txtidx_list,None)
            translated_sent_baseline = translator2.translate_base(src_txtidx_list,0)   
            res_baseline = " ".join(translated_sent_baseline)
            res_baseline_beam = " ".join(translated_sent_baseline_beam)
            txt.insert(END, '。')
            pinyin_str  += '.'
            word_str    += '.'
            print('\nPinyin: {}\nW-Trans:{}\n3-waits:{}\ngreedy: {}\nbeam 5: {}\n{}'.format(pinyin_str, word_str, " ".join(translated_sent),
                                                                                                    res_baseline, res_baseline_beam, "".join(src_txt_list)))
        else:
            res_baseline = ""
            res_baseline_beam = ""
    
        if len(translated_sent)>=2 and translated_sent[-1] == "</s>":       # replace tail ".</s>" or "</s>" to "."
            if translated_sent[-2] == ".":
                translated_sent = translated_sent[:-1]
            else:
                translated_sent[-1] = "."
        
    
        res = " ".join(translated_sent)
    
        lbl.configure(text= res, fg="red", anchor=W, justify=LEFT)
        lbl_baseline.configure(text= res_baseline, fg="blue", anchor=W, justify=LEFT)
        lbl_baseline_beam.configure(text= res_baseline_beam, fg="blue", anchor=W, justify=LEFT)
        lbl_s.configure(text= pinyin_str, fg="black", anchor=W, justify=LEFT)
        lbl_gs.configure(text= word_str, fg="black", anchor=W, justify=LEFT)
        window.update()
        return chn_str, pinyin_str, word_str
    """
        

    def trans_whole(src_txt_list, finishall=False):

        # 3 lines of Chn, pinyin, word_trans, with alignment
        pinyin_list = [pinyin.get(i) for i in src_txt_list]
        out_word_list = [word_translator.translate(i,dest='en').text.lower() for i in src_txt_list]
        
        BLANK = ' '
        chn_str, pinyin_str, word_str = '','',''
        for a,b,c in zip(src_txt_list, pinyin_list, out_word_list):
            if ord(a[0]) > 122:
                x,y,z = count_space(len(a), len(b), len(c))
                chn_str     += a + chr(12288)*(x>>1) #chr(12288)
                pinyin_str  += b + BLANK*y
                word_str    += c + BLANK*z
            else:
                chn_str     += a + '\t'
                pinyin_str  += b + '\t'
                word_str    += c + '\t'
        #pinyin_str = pinyin_str.replace('。','.')
        #word_str   = word_str.replace('。','.')
    
        txt.delete(0,END)
        txt.insert(0, chn_str)
        window.update()

        # end of 3 lines

        src_txtidx_list = []
        res_baseline = ""
        res = ""
        # last version
        #src_txtidx_list = [translator.fields["src"].vocab.stoi[x] for x in src_txt_list]
        #translated_sent = translator.translate_batch(src_txtidx_list,translator.model_opt.waitk) # input is word id list
        
        # 8/8/2018 version
        if len(src_txt_list) >= translator.waitk:            
            current_input_bpe_lst = bpe.segment(" ".join(src_txt_list)).split()            
            src_txtidx_list = []
            #for wordi in current_input_bpe_lst:
            #    src_txtidx_list.append(translator.fields["src"].vocab.stoi[wordi])
            src_txtidx_list = [translator.fields["src"].vocab.stoi[wordi] for wordi in current_input_bpe_lst]

            ### detect the end of sentence
            #if sentwidx == len(sampleWholeInput)-1:
            translated_sent, pre_tgt[0] = translator._translate_batch(src_txtidx_list,pre_tgt[0],freedecoding[0],finishall=finishall)
            res = clean_eos(translated_sent)

            if finishall:
                res_baseline_list, pre_tgt[0] = translator._translate_batch(src_txtidx_list,pre_tgt[0],freedecoding=True,finishall=True)
                #res_baseline = " ".join(clean_eos(res_baseline_list)).replace("@@ ","") + '.'
                res_baseline = clean_eos(res_baseline_list)
                pinyin_str += '。'
                word_str += '.'
                txt.insert(END, '。')
            freedecoding[0] = False            

        lbl_s.configure(text= pinyin_str, fg="black", anchor=W, justify=LEFT)
        lbl_gs.configure(text= word_str, fg="black", anchor=W, justify=LEFT)
        lbl.configure(text= res, fg="red", anchor=W, justify=LEFT)
        lbl_baseline.configure(text= res_baseline, fg="blue", anchor=W, justify=LEFT)
        #lbl_baseline_beam.configure(text= res_baseline_beam, fg="blue", anchor=W, justify=LEFT)
        window.update()

        print('temp: {} {} {} [{} {}] {}'.format(src_txt_list, res, src_txtidx_list, freedecoding[0], finishall, pre_tgt[0]))
        #print('\rChinese: {}'.format(chn_str), end = '')
        if finishall:
            print('\n\nChinese: {}'.format(chn_str))
            print('Pinyin: {}\nW-Trans:{}\nk-waits:{}\ngreedy: {}\n{}'.format(pinyin_str, word_str, 
                                                                                res, res_baseline, "".join(src_txt_list)))

    def clean_eos(s):
        if len(s)>=2 and s[-1] == "</s>":       # replace tail ".</s>" or "</s>" to "."
            if s[-2] == ".":
                s = s[:-1]
            else:
                s[-1] = "."
        #res = " ".join(translated_sent)
        return " ".join(s).replace("@@ ","")

    ## kaibo: flush current line for next sentence with additional new word, until printing the final transcript if `is_final`
    def listen_print_loop(responses):
        """Iterates through server responses and prints them.

        The responses passed is a generator that will block until a response
        is provided by the server.

        Each response may contain multiple results, and each result may contain
        multiple alternatives; for details, see https://goo.gl/tjCPAU.  Here we
        print only the transcription for the top alternative of the top result.

        In this case, responses are provided for interim results as well. If the
        response is an interim one, print a line feed at the end of it, to allow
        the next result to overwrite it, until the response is a final one. For the
        final one, print a newline to preserve the finalized transcription.
        """
        num_words = 0   # number of words in the whole sentence (maybe multi lines)
        num_chars = 0   # number of char in current line
        num_chars_printed = 0
        base_txt_list, txt_list = [], None
        chn_str, pinyin_str, word_str = '','',''
        for response in responses:
            if not response.results:
                continue

            # The `results` list is consecutive. For streaming, we only care about
            # the first result being considered, since once it's `is_final`, it
            # moves on to considering the next utterance.
            asr_result = response.results[0]
            if not asr_result.alternatives:
                continue

            # Display the transcription of the top alternative.
            transcript = asr_result.alternatives[0].transcript
            if len(transcript) > num_chars and ord(transcript[-1]) > 128:   # kaibo: cut only if characters increases & not eng
                num_chars = len(transcript)
                transcript_cut = jieba.cut(transcript, cut_all=False)
                #txt_list = list(w.lower() if ord(w[0])<128 else w for w in chain(base_txt_list, transcript_cut))
                txt_list = list(map(lambda x: x.lower(), chain(base_txt_list, transcript_cut)))
                if len(txt_list) > num_words: # kaibo: update GUI only if words increases
                    num_words = len(txt_list)
                    if num_words > 1:
                        #chn_str, pinyin_str, word_str = trans(txt_list[:-1], chn_str, pinyin_str, word_str)
                        trans_whole(txt_list[:-1])
            '''
            if result.is_final:
                base_txt_list = txt_list
                num_chars = 0
                if txt_list[-1] == '句号':
                    txt_list[-1] = '。'
                    trans(txt_list)                
            '''
            # another solution of ending
            if asr_result.is_final:
                #trans(txt_list + ['。'], chn_str, pinyin_str, word_str)
                #trans_whole(txt_list + ['。'])
                trans_whole(txt_list, finishall=True)
                stream_end[0] = True
                print(". [end]")
                return


    stream_end = [False]
    # Audio recording parameters
    RATE = 16000
    #RATE = 10000
    CHUNK = int(RATE / 10)  # 100ms
    language_code = "cmn-Hans-CN"
    client = speech.SpeechClient()
    config = types.RecognitionConfig(
        encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
        sample_rate_hertz=RATE,
        language_code=language_code)
    streaming_config = types.StreamingRecognitionConfig(
        config=config,
        interim_results=True)


    def clicked(a):
        stream_end[0] = False
        freedecoding[0] = True   # kaibo: added for 8/8/2018 version
        pre_tgt[0] = []         # kaibo: added for 8/8/2018 version
        src_txt_list = []
        clearUI()
        print('say something until a pause as ending in 65 seconds')

        with MicrophoneStream(RATE, CHUNK) as stream:
            audio_generator = stream.generator()
            requests = (types.StreamingRecognizeRequest(audio_content=content)
                        for content in audio_generator)
            responses = client.streaming_recognize(streaming_config, requests)

            # Now, put the transcription responses to use.
            listen_print_loop(responses)
            if stream_end[0]: return
            
    def clear_entry(a):
        src_txt_list = []
        txt.delete(0,END)

    window.bind(act_click, clicked)
    #window.bind('<Enter>', clear_entry)
    
    
    btn = Button(window, text="Click Me")#, command=clicked,fg="red")

    btn.bind('<Button-1>', clicked)    
    
    #btn.grid(column=2, row=0)
    
    window.mainloop()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='translate.py',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    onmt.opts.add_md_help_argument(parser)
    onmt.opts.translate_opts(parser)

    opt = parser.parse_args()
    print(opt.model)
    logger = init_logger(opt.log_file)
    main(opt)
