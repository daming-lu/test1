var modelLoaded = false;
var timeCounting = true;
function update_cpu_load() {
    
    //$.getJSON($SCRIPT_ROOT+"/_stuff",
    $.getJSON("/cpu",
        function(data) {
            $("#cpu").html(data.cpu+" %");
            $("#ram").html(data.ram+" % ("+(''+(parseFloat(data.ram)*0.16)).slice(0,5) + " G) / 16 G");
            $("#time").html(data.time) ;
        });
}

//console.log('in js');



// click to start asr & mt
$('#startBtn').bind('click', function() {
  console.log('start');
  timeCounting = true;
  modelLoaded = true;
  //$.getJSON($SCRIPT_ROOT + '/start', 
  $.getJSON("/start", 
    function(data) {
      console.log('start2');
        //$("#modelLoadingMsg").html("model: "+data.model);
  });
  modelLoaded = true;
});


// click to stop asr & mt
$('#stopBtn').bind('click', function() {
  console.log('stop');
  timeCounting = false;
  modelLoaded = false;
});

// update the stream result in screen
function fillScreen() {
    if (!modelLoaded){return;}
    //$.getJSON($SCRIPT_ROOT+"/_stuff",
    $.getJSON("/fillScreen",
        function(data) {
          $("#modelLoadingMsg").html(data.modelStatus);
          if (timeCounting){
            if ((data.duration) < 65){
                $("#duration").text((''+data.duration).slice(0,4) + 's / 65s');    
            }else{
                $("#duration").text('65s / 65s');
            }
          }
          

          $("#chnLine").html(data.Line_chn);
          $("#pynLine").html(data.Line_pyn);
          $("#wrdLine").html(data.Line_wrd);

          var chnBox = $("#chnBox");
          var engBox = $("#engBox");
          var bslBox = $("#bslBox");
          chnBox.html(data.BoxDisplay_chn);
          engBox.html(data.BoxDisplay_eng);
          bslBox.html(data.BoxDisplay_bsl);
          if (data.scroll_chn > 0) { chnBox.animate({scrollTop:32*data.scroll_chn});  }//21
          if (data.scroll_eng > 0) { engBox.animate({scrollTop:24*data.scroll_eng});  }//18
          if (data.scroll_bsl > 0) { bslBox.animate({scrollTop:24*data.scroll_bsl});  }//18
          //console.log((data.scroll_chn+7) + '/' + (data.scroll_eng+7) + '/' + (data.scroll_bsl+7))
          $('#link').html((data.scroll_chn) + '/' + (data.scroll_eng) + '/' + (data.scroll_bsl) + ' --- ' + chnBox.scrollTop() + '/' + engBox.scrollTop());

        });

}


setInterval(update_cpu_load, 1000);
setInterval(fillScreen, 300);


