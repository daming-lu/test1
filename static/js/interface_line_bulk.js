var modelLoaded = false;
var timeCounting = true;

function update_cpu_load() {

    //$.getJSON($SCRIPT_ROOT+"/_stuff",
    $.getJSON("/cpu",
        function(data) {
            $("#cpu").html(data.cpu + " %");
            $("#ram").html(data.ram + " % (" + ('' + (parseFloat(data.ram) * 0.16)).slice(0, 5) + " G) / 16 G");
            $("#time").html(data.time);
        });
}

//console.log('in js');



// click to start asr & mt
$('#startBtn').bind('click', function() {
    console.log('start');
    timeCounting = true;
    modelLoaded = true;
    //$.getJSON($SCRIPT_ROOT + '/start',
    $.getJSON("/start",
        function(data) {
            console.log('start2');
            //$("#modelLoadingMsg").html("model: "+data.model);
        });
    modelLoaded = true;
});


// click to stop asr & mt
$('#stopBtn').bind('click', function() {
    console.log('stop');
    timeCounting = false;
    modelLoaded = false;
});

var sentenceSoFar = '',
    isPlaying = false,
    polls = 0;

//var audio = new Audio();
var audio;
audio = new Audio();

var baiduTtsPrefix = 'http://tsn.baidu.com/text2audio?tex=';
var baiduTtsSuffix = '&tok=24.960c66ace78789a9a389649a5e936bc9.2592000.1543099285.282335-14336126&lan=zh&aue=3&ctp=1&cuid=123456PYTHON&vol=5&pit=5&spd=7&per=0';

var wordsList = [];
// update the stream result in screen
function fillScreen() {
    if (!modelLoaded) {
        sentenceSoFar = '';
        isPlaying = false;
        return;
    }
    //$.getJSON($SCRIPT_ROOT+"/_stuff",
    $.getJSON("/fillScreen",
        function(data) {
            $("#modelLoadingMsg").html(data.modelStatus);
            if (timeCounting) {
                if ((data.duration) < 65) {
                    $("#duration").text(('' + data.duration).slice(0, 4) + 's / 65s');
                } else {
                    $("#duration").text('65s / 65s');
                }
            }
//            console.log('data');
//            console.log(data);
            var curEngSentence = data['BoxDisplay_eng'];
            var delta = curEngSentence.substring(sentenceSoFar.length);
            console.log('delta');
            console.log(delta);
            if (delta.length > 7 && !isPlaying) {
                wordsList.push(delta);
//                isPlaying = true;

                sentenceSoFar = curEngSentence;
//                if (!audio) {
//                }

                // Baidu TTS
                var deltaForUrl = delta.split(' ').join('%2B');
                var curUrl = baiduTtsPrefix + deltaForUrl + baiduTtsSuffix;

                audio.addEventListener('ended', function () {
                    if (wordsList.length > 2) {
                        var delta = wordsList.shift();
                        var deltaForUrl = delta.split(' ').join('%2B');
                        var delta2 = wordsList.shift();
                        var delta2ForUrl = delta2.split(' ').join('%2B');

                        var curUrl = baiduTtsPrefix + deltaForUrl + '%2B' + delta2ForUrl + baiduTtsSuffix;
                        audio.src = curUrl;
                        audio.play();
                    }
                    isPlaying = false;
                }, false);

                audio.src = curUrl;
                audio.play();
//                isPlaying = false;
                polls += 1;
                isPlaying = true;

            }



//            audio.addEventListener('ended', function () {
//                audio.src = url1;
//                audio.play();
//            }, false);
//            audio.volume = 0.3;
//            audio.loop = false;
//            audio.src = playlist[0];
//            audio.play();

            //$("#chnLine").html(data.Line_chn);
            //$("#pynLine").html(data.Line_pyn);
            //$("#wrdLine").html(data.Line_wrd);

            var dispBox1 = $("#chnBox");
            var dispBox2 = $("#engBox");
            dispBox1.html(data.BoxDisplay_chn);
            dispBox2.html(data.BoxDisplay_eng);
            //if (data.scroll_chn > 0) { chnBox.animate({scrollTop:32*data.scroll_chn});  }//21
            //if (data.scroll_eng > 0) { engBox.animate({scrollTop:24*data.scroll_eng});  }//18
            //if (data.scroll_bsl > 0) { bslBox.animate({scrollTop:24*data.scroll_bsl});  }//18
            //console.log((data.scroll_chn+7) + '/' + (data.scroll_eng+7) + '/' + (data.scroll_bsl+7))
            $('#link').html(
                (data.scroll_chn) + '/' + (data.scroll_eng) + '/' + (data.scroll_bsl)
                //            + ' --- ' + chnBox.scrollTop() + '/'
                //            + engBox.scrollTop()
            );

        });

}


setInterval(update_cpu_load, 1000);
setInterval(fillScreen, 750);